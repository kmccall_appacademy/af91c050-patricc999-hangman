class Hangman

  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    length = referee.pick_secret_word
    guesser.register_secret_length(length)
    @board = Array.new(length, "_")
  end

  def take_turn
    letter = guesser.guess
    referee.check_guess(letter)
    update_board(letter)
    guesser.handle_response
  end

  def update_board(letter)
  end

end

class HumanPlayer

  def register_secret_length(length)
    puts 'The length of the word is:' + length.to_s
  end

end

class ComputerPlayer

  attr_reader :dictionary, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    dictionary.first.length
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def check_guess(guess)
    correct_guess = []

    if dictionary[0].include?(guess)
      dictionary[0].each_char.with_index do |char, idx|
        correct_guess.push(idx) if char == guess
      end
    else
      []
    end
    correct_guess
  end

  def guess(board)
    letter_count = Hash.new(0)
    words = candidate_words.join
    words.chars.each do |letter|
      if !board.include?(letter)
        letter_count[letter] += 1
      end
    end
    sorted_count = letter_count.sort_by { |_key, value| value }
    sorted_count.last.first
  end

  def handle_response(letter, board)
    if board == []
      candidate_words.reject! { |k, _v| k.chars.include?(letter) }
    else
      board.each do |idx|
        candidate_words.reject! do |word|
          word[idx] != letter
        end
        candidate_words.reject! do |word|
          word.chars.count(letter) != board.count
        end
      end
      # candidate_words.select! { |k, _v| k.chars.include?(letter) }
    end
  end

end
